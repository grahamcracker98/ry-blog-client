import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Profile } from '../profile';
import { Registration } from '../registration';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Post } from '../post';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(
    private appService: AppService, 
    private router: Router,
    private route: ActivatedRoute
    ) { }

  public isLoggedIn: boolean;

  message: string = '';

  // allPosts: Post[];

  // myPosts: Post[];

  postView: string;

  displayPosts: Post[];

  // page = 0;

  // pageSize = 5;

  ngOnInit(){
    // Populate post feed
    //this.getPosts();
    this.appService.getAllPosts().subscribe((data: Post[]) => this.displayPosts = data['content']);

    // Display message based on url
    this.setMessage(this.route.snapshot.queryParamMap.get("message"));

    // Set isLoggedIn if access token exists
    this.isLoggedIn = this.appService.checkCredentials();
    
    // Retrieve access token if redirected from user API
    let i = window.location.href.indexOf('code');
    if (!this.isLoggedIn && i != -1){
        this.appService.retrieveToken(window.location.href.substring(i + 5));
    }
  }

  login() {
    window.location.href = environment.userApiUrl + 'oauth/authorize?response_type=code&client_id=' + environment.clientId + '&redirect_uri='+ environment.redirectUri;
  }

  signup() {
    window.location.href = environment.userApiUrl + 'signup';
  }

  logout() {
    this.appService.logout();
  }

  // getPosts(){
  //   this.appService.getAllPosts().subscribe((data: Post[]) => this.allPosts = data['content']);
    // if (this.isLoggedIn){
    //   this.myPosts = this.appService.getMyPosts();
    // }
  //}

  // prev(){
  //   this.page -=1;
  //   this.appService.getPosts(this.page, this.pageSize).subscribe(
  //     (data: Post[]) => {
  //       this.allPosts = data['content'];
  //       console.log(this.allPosts);
  //       this.setPostView(this.postView);
  //     }
  //   );
  // }

  // next(){
  //   this.page +=1;
  //   this.appService.getPosts(this.page, this.pageSize).subscribe(
  //     (data: Post[]) => {
  //       this.allPosts = data['content'];
  //       console.log(this.allPosts);
  //       this.setPostView(this.postView);
  //     }
  //   );
  // }

  setMessage(message: string){
    switch(message) { 
      case 'logout': { 
        this.message = 'You have been logged out.'; 
        break;
      } 
      case 'signup': { 
        this.message = 'Thanks for registering! Make sure to confirm your email address!'; 
        break;
      } 
      case 'verified': { 
        this.message = 'Thank you for verifying your email address!'; 
        break;
      } 
      case 'expired': { 
        this.message = 'Email verification link expired.'; 
        break;
      } 
      default: {
        this.message = ''; 
        break;
      } 
    }
  }
  
  // setPostView(postView: string){
  //   // if (this.postView != postView){
  //   //   this.page = 0;
  //   // }
  //   this.postView = postView;
  //   switch(postView){
  //     case 'all': {
  //       this.displayPosts = this.allPosts;
  //       break;
  //     }
  //     case 'my': {
  //       this.displayPosts = this.myPosts;
  //       break;
  //     }
  //   }
  // }
}
