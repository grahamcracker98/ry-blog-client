export const environment = {
  production: true,
  userApiUrl: 'https://dashboard.heroku.com/apps/grahamja-user/',
  postApiUrl: 'http://localhost:8081/',
  blogApiUrl: 'http://localhost:8081/',
  clientId: 'heroku',
  clientSecret: 'secret',
  baseUrl: 'http://localhost:4200/dashboard',
  redirectUri: 'http://localhost:4200/dashboard'
};
